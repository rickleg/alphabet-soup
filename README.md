# Griffin Rickle's Solution

## Technologies
Developed using Sublime Text 3 on Fedora 29 with Gradle as a build tool and JUnit / Mockito as a testing framework

## Techniques
The main technique here is seen in the `GridSpecController` class, which uses recursion to find the words within the given grid. This could have been done iteratively, using a while loop to search for the words within the grid, but I thought that using recurusion was not only more elegant, but also showcased my skills. I do not use a Windows machine for development, but I made my best effort to create a solution which could be run on either Windows, Mac, or Linux. Really, this boils down to:

1. Using System.getProperty("file.separator") character and the Paths interface when building paths.
2. Using the Gradle Wrapper (./gradlew on Linux and gradle.bat on Windows).

## HowTo
Using gradle, we can take advantage of the preconfigured tasks it has for the Java and Application plugins.

All commands should be run from the top-level directory of the project.

### To run all tests:
Linux:
```
./gradlew test
```
Windows:
```
gradle.bat test
```

### To process a file using the program:
Linux:
```
./gradlew run --args="path/to/file"
```
Windows:
```
gradle.bat run --args="path\to\file"
```

where "path/to/file" is the relative path to the file you wish to process. The sample file can be processed by issuing the command

```
./gradlew run --args="src/test/resources/sample.txt"
```

# Alphabet Soup

> You have been contracted from a newspaper tasked with the job of providing an answer key to their word search for the Sunday print. The newspaper's word search is a traditional game consisting of a grid of characters in which a selection of words have been hidden. You are provided with the list of words that have been hidden and must find the words within the grid of characters. 

## Requirements
Load a character grid with scrambled words embedded within it and a words list of the words to find.  The following conditions apply:

- Within the grid of characters, the words may appear vertical, horizontal or diagonal.
- Within the grid of characters, the words may appear forwards or backwards. 
- Words that have spaces in them will not include spaces when hidden in the grid of characters.

### Input Format
The program is to accept a file as input. The file is an ASCII text file containing the word search board along with the words that need to be found. 

The file contains three parts. The first part is the first line, and specifies the number of rows and columns in the grid of characters, separated by an 'x'. The second part provides the grid of characters in the word search. The third part in the file specifies the words to be found.

The first line indicates how many following lines in the file contain the rows of characters that make up the word search grid. Each row in the word search grid will have the specified number of columns of characters, each separated with a space. The remaining lines in the file specify the words to be found.

The file format is as follows:

```
3x3
A B C
D E F
G H I
ABC
AEI
```

You may assume that the input files are correctly formatted. Error handling for invalid input files may be ommitted.

### Output Format
The output will specify the word found, along with the indices specifying where the beginning and ending characters of the word are located in the grid. A single space character will separate the word from the beginning and ending indices. The order of the words in the output should remain the same as the order of the words specified in the input file. The program will output to screen or console (and not to a file). 

The word search grid row and column numbers can be used to identify the location of individual characters in the board. For example, row 0 column 0 (represented as `0:0`) is the top-left character of a 3x3 board.  The bottom-right corner of a 3x3 board would be represented as `2:2`.

```
ABC 0:0 0:2
AEI 0:0 2:2
```

## Sample Data
The following may be used as sample input and output datasets.

### Input

```
5x5
H A S D F
G E Y B H
J K L Z X
C V B L N
G O O D O
HELLO
GOOD
BYE
```

### Ouput

```
HELLO 0:0 4:4
GOOD 4:0 4:3
BYE 1:3 1:1
```
