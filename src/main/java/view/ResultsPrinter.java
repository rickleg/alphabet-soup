package view;

import java.util.Map;
import java.util.List;

import java.lang.StringBuilder;

public class ResultsPrinter {
	
	public ResultsPrinter(){}

	public String getString(Map<String, int[][]> results, List<String> orderedWords){
		StringBuilder sb = new StringBuilder();
		for(String key : orderedWords) {
			int[][] position = results.get(key);
			if(position != null){
				sb.append(key).append(" ").append(position[0][0]).append(":").append(position[0][1]).append(" ").append(position[1][0]).append(":").append(position[1][1]);
			}
			sb.append("\n");
		}
		return sb.toString();
	}
}