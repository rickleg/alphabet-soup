package driver;

import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.Paths;

import java.io.IOException;

import java.util.Map;
import java.util.List;

import java.lang.StringBuilder;

import view.ResultsPrinter;
import controller.GridSpecController;

public class Driver {
	
	// Controller which does most of the processing w/ the GridSpec
	private static GridSpecController gridSpecController;

	/**
	 * Method which kicks everything off!
	 * 
	 * @param args Array whose only element is the path to the input file
	*/
	public static void main(String[] args){
		
		// Validate that there is only one argument
		if(args.length != 1) {
			throw new RuntimeException("Number of arguments must be equal to one");
		}

		// Validate that the file actually exists
		String inputFileString = args[0];
		Path inputFilePath = Paths.get(inputFileString);
		if(!Files.exists(inputFilePath)){
			throw new RuntimeException("File does not exist at location " + inputFilePath);
		}

		// There was only one argument and the file exists, so read all lines.
		List<String> inputContents;
		try {
			inputContents = Files.readAllLines(inputFilePath);
		} catch(IOException ioe) {
			throw new RuntimeException("IOException caught while reading input file; not handling");
		}

 		// Create a new GridSpecController to perform operations on the input
		if (gridSpecController == null) {
			gridSpecController = new GridSpecController(inputContents);
		}

		// Create holder for results and tell the gridSpecController to get to work!
		Map<String, int[][]> results = gridSpecController.findWords();
		ResultsPrinter printer = new ResultsPrinter();
		System.out.println(printer.getString(results, gridSpecController.getGridSpec().getSearchWords()));
	}

	// Protected method so only packages in the controller package can call this method (AKA Tests)
	protected void setGridSpecController(GridSpecController gsc) {
		this.gridSpecController = gsc;
	}

}