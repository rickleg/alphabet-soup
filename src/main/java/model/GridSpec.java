package model;

import java.util.stream.Collectors;

import java.util.Map;
import java.util.List;
import java.util.Arrays;
import java.util.HashMap;
import java.util.ArrayList;

import org.apache.commons.lang3.ArrayUtils;

/**
 * Data model for an input file
*/
public class GridSpec {

	int rows;
	int columns;

	Map<Character, int[][]> characterMap;
	char[][] grid;

	List<String> searchWords;

	/**
	 * Constructor
	 *
	 * @param input List of Strings representing lines in an input file
	*/
	public GridSpec(List<String> input) {
		// Read first line and get dimensions
		String dimensions = input.get(0);
		String[] rowColStrings = dimensions.split("x");

		// Let autoboxing/unboxing take care of converting values
		rows = Integer.valueOf(rowColStrings[0]);
		columns = Integer.valueOf(rowColStrings[1]);

		// Read in the actual grid specified on lines 1 -> rows + 1
		List<String> gridList = input.subList(1, rows + 1);
		// Create new 2d char array to hold grid
		grid = new char[rows][];
		// Loop through grid
		for(int rowCounter = 0; rowCounter < rows; rowCounter++){
			grid[rowCounter] = new char[columns];
			// Each cell in the row is space-separated
			String[] currentRow = gridList.get(rowCounter).split(" ");
			for(int colCounter = 0; colCounter < columns; colCounter++) {
				/*
				Can assume that input files are well-formed, so we know that each cell is just 1 letter
				Thus, we can just take the first character of each "cell" since each cell is a 1-letter string.
				*/
				grid[rowCounter][colCounter] = currentRow[colCounter].toCharArray()[0];
			}
		}

		// Create map of where each letter occurs so we know where to start searching
		characterMap = processCharacterMap(grid);

		// Create list of words to search for, specified in input file after the grid.
		searchWords = new ArrayList<String>();
		searchWords.addAll(input.subList(rows + 1, input.size()));
	}

	/**
	 * Creates Character to position in grid mapping for each character in the grid
	 * 
	 * @param grid Grid which was specified by input file
	*/
	protected Map<Character, int[][]> processCharacterMap(char[][] grid){
		// Create data structure
		Map<Character, int[][]> toReturn = new HashMap<>();
		// Loop through row of grid
		for(int rowCounter = 0; rowCounter < grid.length; rowCounter++) {
			// Grab current row and loop through it
			char[] currentRow = grid[rowCounter];
			for(int colCounter = 0; colCounter < currentRow.length; colCounter++) {
				// Grab current character and take not of its position
				char currentChar = currentRow[colCounter];
				int[][] charPosition = new int[1][];
				charPosition[0] = new int[] {rowCounter, colCounter};
				if (!toReturn.keySet().contains(currentChar)) {
					// If the toReturn hashmap doesn't have it, this is a new character we're processing.
					// Create the 2d array necessary and assign it as the value to the character key.
					toReturn.put(currentChar, charPosition);
				} else {
					// Else, the toReturn hashmap DOES have it, so grab what's already there and add the
					// current position to that array.
					int[][] currPositions = toReturn.get(currentChar);
					int[][] updatedPositions = (int[][])ArrayUtils.addAll(currPositions, charPosition);
					toReturn.put(currentChar, updatedPositions);
				}
			}
		}
		return toReturn;
	}

	// Standard getters
	
	public int getRows(){
		return rows;
	}

	public int getColumns(){
		return columns;
	}

	public char[][] getGrid(){
		return grid;
	}

	public List<String> getSearchWords(){
		return searchWords;
	}

	public Map<Character, int[][]> getCharacterMap(){
		return characterMap;
	}
}