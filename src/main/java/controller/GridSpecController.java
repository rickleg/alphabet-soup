package controller;

import model.GridSpec;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Collectors;

/**
 * Controller responsible for driving processing on an input file
*/
public class GridSpecController {

	private GridSpec gridSpec;

	/**
	 * Constructor for an already created GridSpec object
	*/
	public GridSpecController(GridSpec gridSpec) {
		this.gridSpec = gridSpec;
	}

	/**
	 * Constructor for a list.
	*/
	public GridSpecController(List<String> input) {
		this(new GridSpec(input));
	}

	/**
	 * Enum so the recursive method knows which direction it's searching in
	*/
	enum SearchDirection {

		DOWN(-1, 0, "DOWN"),
		UP(1, 0, "UP"),
		LEFT(0, -1, "LEFT"),
		RIGHT(0, 1, "RIGHT"),
		DIAG_UL(-1, -1, "DIAG_UL"),
		DIAG_UR(-1, 1, "DIAG_UR"),
		DIAG_DL(1, -1, "DIAG_DL"),
		DIAG_DR(1, 1, "DIAG_DR");

		private final int rowChange;
		private final int colChange;
		private final String directionString;
		private SearchDirection(int rowChange, int colChange, String directionString){
			this.rowChange = rowChange;
			this.colChange = colChange;
			this.directionString = directionString;
		}
		public int getRowChange() {
			return rowChange;
		}
		public int getColChange() {
			return colChange;
		}
		public String getDirectionString() {
			return directionString;
		}
	}

	/**
	 * The output map is of the form:
     * {
	 *   String_To_Find -> [ [starting_row, starting_column], [ending_row, ending_column] ]
     * }
     * This method pretty much just grabs the needed stuff from the GridSpec, loops through the words
     * it's supposed to find, and grabs the output from the search method.
     * 
     * @return A Mapping of Strings to their starting and ending positions
	*/
	public Map<String, int[][]> findWords(){
		// What do you expect me to do with an empty gridSpec?
		if(gridSpec == null) {
			throw new RuntimeException("Cannot run without a GridSpec!");
		}

		// Grab specs of the puzzle from the gridSpec
		char[][] currGrid = gridSpec.getGrid();
		List<String> wordsToFind = gridSpec.getSearchWords();
		Map<Character, int[][]> characterMap = gridSpec.getCharacterMap();

		// To return the output we find!	
		Map<String, int[][]> toReturn = new HashMap<>();

		// Loop through words to find and find each one.
		for(String wordToFind : wordsToFind) {
			int[][] coordinates = search(gridSpec, characterMap, wordToFind);
			toReturn.put(wordToFind, coordinates);
		}

		return toReturn;
	}

	/**
	 * Method which massages data and gets things ready for the recursive jump.
	 * 
	 * @param gridSpec     The GridSpec this method should use to find the single word this method is to find
	 * @param characterMap Map of characters to their positions within the grid. Used to find starting positions to search for each word.
	 * @param wordToFind   The word we're currently searching for
	 * @return 			   Array of the form [ [startingRow, startingCol], [endingRow, endingCol] ] specifying the beginning and end of the word within the grid.
	*/
	protected int[][] search(GridSpec gridSpec, Map<Character, int[][]> characterMap, String wordToFind){
		// Grab first char and get its positions in the grid
		char startingChar = wordToFind.charAt(0);
		int[][] startingCharPositions = characterMap.get(startingChar);

		// Iterate through all the positions in the grid and search all directions for the rest of the word using the recursiveSearch method
		for(int positionCounter = 0; positionCounter < startingCharPositions.length; positionCounter++) {
			int[] currentPosition = startingCharPositions[positionCounter];
			int currentRow = currentPosition[0];
			int currentCol = currentPosition[1];
			
			// Iterate through each direction and send the recursive method off on each one
			for (SearchDirection direction : SearchDirection.values()) {
				/* According to the spec:
				   Words that have spaces in them will not include spaces when hidden in the grid of characters.
				   Then we need to replace all space characters in the string with empty string
				*/
				int[] currentSearchCoordinates = recursiveSearch(gridSpec.getGrid(), direction, currentRow, currentCol,
					                                             gridSpec.getRows(), gridSpec.getColumns(), wordToFind.replace("\t", "").replace(" ", ""));
				if(currentSearchCoordinates != null) {
					// If we found it, stop processing; we got it!
					return new int[][] { currentPosition, currentSearchCoordinates };
				}
			}
		}
		// Many sad face. Input is supposed to be all well formed so we shouldn't have to worry about this at all.
		System.out.println("Didn't find word " + wordToFind);
		return null;
	}

	/**
	 * Recursive method which does the meat of the processing!
	 *
	 * @param grid       2d char array specifying the grid we're searching for a word within
	 * @param direction  Enum which dictates which direction to move within the grid
	 * @param currentRow int specifying the row we're currently processing
	 * @param currentCol int specifying the column we're currently processing
	 * @param numRows    int specifying the number of rows in the grid (passed so we don't keep having to call grid.length)
	 * @param numCols    int specifying the number of columns in the grid (passed so we don't keep having to call grid[row].length)
	 * @param wordToFind String of characters which we're searching for within the grid. Gets eaten away as we recurse through the grid.
	 * @return null if word was not found in the specified direction. 2-element array of [ row, column ] specifying where the word ends if the word was found. 
	*/
	private int[] recursiveSearch(char[][] grid, SearchDirection direction, int currentRow, int currentCol, int numRows, int numCols, String wordToFind){
		if(wordToFind.length() == 0 || currentRow >= numRows || currentCol >= numCols || currentRow < 0 || currentCol < 0) {
			/* Validity conditions.
			   - If the word has no length, why are we here and what are we searching for? Invalid input and return null.
			   - If currentRow >= numRows or currrentCol >= numCols, we are out of bounds. What are we doing here? return null.
			*/
			return null;
		} else if (wordToFind.length() == 1) {
			/*
			  The real fun stuff; the base case!
			  - If we're here, then we're on the last character of the word we're supposed to find/
			  - This means that if the character specified at [ currentRow, currentCol ] equals the only character left of the
			    word, we've found the word!
			  - If the character doesn't match, though, it was close but no cigar!
			*/
			if(grid[currentRow][currentCol] == wordToFind.charAt(0)){
				return new int[] {currentRow, currentCol};
			} else {
				return null;
			}
		} else {
			/*
			  The recursive step!
			  - If the next character of the word matches our current position, we're in; adjust the searchRow and searchCol according to the
			    direction we're iterating in and search for the current word minus the first character in that direction.
			  - If the next character doesn't match no big deal; we didn't find the word on this run!
			*/
			if(grid[currentRow][currentCol] == wordToFind.charAt(0)){
				return recursiveSearch(grid, direction, currentRow + direction.getRowChange(),
					                   currentCol + direction.getColChange(), numRows, numCols, wordToFind.substring(1));
			} else {
				// Current character didn't match.... Too bad so sad.
				return null;
			} 
		}
	}

	// Protected method so only packages in the controller package can call this method (AKA Tests)
	protected void setGridSpec(GridSpec gridSpec) {
		this.gridSpec = gridSpec;
	}

	public GridSpec getGridSpec(){
		return this.gridSpec;
	}

}