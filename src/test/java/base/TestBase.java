package base;

import java.nio.file.Files;
import java.nio.file.Paths;

import java.io.IOException;

import java.util.Map;
import java.util.List;
import java.util.Arrays;
import java.util.HashMap;

import java.util.stream.Collectors;

import model.GridSpec;

/**
 * Class which is the base for all the tests; has mostly utility methods to get results and whatnot.
*/
public class TestBase {
	
	// returns list<String> representing sample input
	protected List<String> getSampleInput() {
		return getInputFromFile("src", "test", "resources", "sample.txt");
	}

	// returns GridSpec constructed from sample input
	protected GridSpec getSampleGridSpec() {
		return new GridSpec(getSampleInput());
	}

	// returns List<String> representing file whose path is passed in
	protected List<String> getInputFromFile(String... inputFileSpec) {
		try {
			List<String> inputList = Arrays.asList(inputFileSpec);
			// Construct path using System.getProperty("file.separator") so we can run on Windows, too :)
			String inputPathString = inputList.stream().collect(Collectors.joining(System.getProperty("file.separator")));
			return Files.readAllLines(Paths.get(inputPathString));
		} catch (IOException ioe){
			System.err.println("IOException caught while trying to read sample input file:");
			System.err.println(ioe.getMessage());
			System.exit(1);
		} catch (Exception e) {
			System.err.println("General exception caught while trying to read sample input file:");
			System.err.println(e.getMessage());
			System.exit(2);
		}
		return null;
	}

	// returns results for sampe input
	protected Map<String, int[][]> getSampleResults() {
		Map<String, int[][]> toReturn = new HashMap<>();
		toReturn.put("HELLO", new int[][] {new int[] {0, 0}, new int[] {4, 4}});
		toReturn.put("GOOD", new int[][] {new int[] {4, 0}, new int[] {4, 3}});
		toReturn.put("BYE", new int[][] {new int[] {1, 3}, new int[] {1, 1}});
		return toReturn;
	}

	// returns character map for the sample input
	protected Map<Character, int[][]> getSampleCharacterMap() {
		HashMap<Character, int[][]> sampleCharMap = new HashMap<>();
		sampleCharMap.put('A', new int[][] {new int[] {0, 1}});
		sampleCharMap.put('B', new int[][] {new int[] {1, 3}, new int[] {3, 2}});
		sampleCharMap.put('C', new int[][] {new int[] {3, 0}});
		sampleCharMap.put('D', new int[][] {new int[] {0, 3}, new int[] {4, 3}});
		sampleCharMap.put('E', new int[][] {new int[] {1, 1}});
		sampleCharMap.put('F', new int[][] {new int[] {0, 4}});
		sampleCharMap.put('G', new int[][] {new int[] {1, 0}, new int[] {4, 0}});
		sampleCharMap.put('H', new int[][] {new int[] {0, 0}, new int[] {1, 4}});
		sampleCharMap.put('J', new int[][] {new int[] {2, 0}});
		sampleCharMap.put('K', new int[][] {new int[] {2, 1}});
		sampleCharMap.put('L', new int[][] {new int[] {2, 2}, new int[] {3, 3}});
		sampleCharMap.put('N', new int[][] {new int[] {3, 4}});
		sampleCharMap.put('O', new int[][] {new int[] {4, 1}, new int[] {4, 2}, new int[] {4, 4}});
		sampleCharMap.put('S', new int[][] {new int[] {0, 2}});
		sampleCharMap.put('V', new int[][] {new int[] {3, 1}});
		sampleCharMap.put('X', new int[][] {new int[] {2, 4}});
		sampleCharMap.put('Y', new int[][] {new int[] {1, 2}});
		sampleCharMap.put('Z', new int[][] {new int[] {2, 3}});
		return sampleCharMap;
	}

	// Compares two Map<Character, int[][]> 
	protected boolean charMapsEqual(Map<Character, int[][]> map1, Map<Character, int[][]> map2) {
		if(map1.keySet().size() != map2.keySet().size()){
			return false;
		}

		for(Character character : map1.keySet()){ 
			int[][] map1Positions = map1.get(character);
			for(int rowCounter = 0; rowCounter < map1Positions.length; rowCounter++) {
				int[] currentPositions = map1Positions[rowCounter];
				for(int colCounter = 0; colCounter < currentPositions.length; colCounter++){
					if(map1Positions[rowCounter][colCounter] != map2.get(character)[rowCounter][colCounter]) {
						return false;
					}
				}
			}
		}
		return true;
	}
}