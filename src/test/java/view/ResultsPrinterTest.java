package view;

import org.junit.Test;

import java.util.Map;
import java.util.List;
import java.util.Arrays;
import java.util.HashMap;

import model.GridSpec;

import base.TestBase;

import static org.junit.Assert.assertEquals;

public class ResultsPrinterTest extends TestBase {

	@Test
	public void testSampleGetString() {
		List<String> words = Arrays.asList("HELLO", "GOOD", "BYE");
		Map<String, int[][]> sampleResults = getSampleResults();

		String expectedOutput = "HELLO 0:0 4:4\n" +
		                        "GOOD 4:0 4:3\n" +
		                        "BYE 1:3 1:1\n";

		ResultsPrinter printer = new ResultsPrinter();

		assertEquals(expectedOutput, printer.getString(sampleResults, words));
	}

}