package model;

import base.TestBase;

import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Map;
import java.util.List;
import java.util.Arrays;
import java.util.HashMap;
import java.util.ArrayList;

public class GridSpecTest extends TestBase {
	
	@Test
	public void testSample(){
		List<String> sampleInput = getSampleInput();
		GridSpec testGridSpec = new GridSpec(sampleInput);
		assertEquals(5, testGridSpec.getRows());
		assertEquals(5, testGridSpec.getColumns());

		char[][] expectedGrid = new char[5][];
		expectedGrid[0] = new char[] {'H', 'A', 'S', 'D', 'F'};
		expectedGrid[1] = new char[] {'G', 'E', 'Y', 'B', 'H'};
		expectedGrid[2] = new char[] {'J', 'K', 'L', 'Z', 'X'};
		expectedGrid[3] = new char[] {'C', 'V', 'B', 'L', 'N'};
		expectedGrid[4] = new char[] {'G', 'O', 'O', 'D', 'O'};
		assertEquals(expectedGrid, testGridSpec.getGrid());

		List<String> expectedSearchWords = Arrays.asList("HELLO", "GOOD", "BYE");
		assertEquals(expectedSearchWords, testGridSpec.getSearchWords());

		Map<Character, int[][]> expectedMap = getSampleCharacterMap();
		
		Map<Character, int[][]> currentCharacterMap = testGridSpec.getCharacterMap();
		assertTrue(charMapsEqual(expectedMap, currentCharacterMap));
	}

	@Test
	public void testProcessCharacterMap(){
		char[][] testGrid = new char[][] { new char [] { 'A', 'B' }, new char[] {'C', 'D'} };
		GridSpec testGridSpec = new GridSpec(Arrays.asList("2x2", "A B", "C D"));

		Map<Character, int[][]> expectedCharMap = new HashMap<>();
		expectedCharMap.put('A', new int[][] { new int[] {0, 0} });
		expectedCharMap.put('B', new int[][] { new int[] {0, 1} });
		expectedCharMap.put('C', new int[][] { new int[] {1, 0} });
		expectedCharMap.put('D', new int[][] { new int[] {1, 1} });

		Map<Character, int[][]> returnedCharMap = testGridSpec.getCharacterMap();
		assertTrue(charMapsEqual(expectedCharMap, returnedCharMap));

		testGrid[0][1] = 'A';
		testGrid[1][0] = 'A';
		testGrid[1][1] = 'A';
		expectedCharMap = new HashMap<>();
		expectedCharMap.put('A', new int[][] { new int[] {0, 0}, new int[] {0, 1}, new int[] {1, 0}, new int[] {1, 1} });
		testGridSpec = new GridSpec(Arrays.asList("2x2", "A A", "A A"));
		returnedCharMap = testGridSpec.getCharacterMap();
		assertTrue(charMapsEqual(expectedCharMap, returnedCharMap));
	}
}