package controller;

import base.TestBase;

import model.GridSpec;

import java.util.Map;
import java.util.Arrays;

import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

public class GridSpecControllerTest extends TestBase {

	@Test
	public void testInstantiation() {
		GridSpecController gridSpecController = new GridSpecController(getSampleInput());
		GridSpec currGridSpec = gridSpecController.getGridSpec();

		assertEquals(currGridSpec.getRows(), 5);
		assertEquals(currGridSpec.getColumns(), 5);
		assertEquals(currGridSpec.getSearchWords(), Arrays.asList("HELLO", "GOOD", "BYE"));
		
	}

	@Test
	public void testProcessSampleInput() {
		GridSpecController gridSpecController = new GridSpecController(getSampleInput());
		Map<String, int[][]> results = gridSpecController.findWords();
		assertTrue(results.keySet().contains("HELLO"));
		assertTrue(results.keySet().contains("GOOD"));
		assertTrue(results.keySet().contains("BYE"));
		assertEquals(results.keySet().size(), 3);

		int[][] helloPositions = results.get("HELLO");
		assertEquals(helloPositions[0][0], 0);
		assertEquals(helloPositions[0][1], 0);
		assertEquals(helloPositions[1][0], 4);
		assertEquals(helloPositions[1][1], 4);

		int[][] goodPositions = results.get("GOOD");
		assertEquals(goodPositions[0][0], 4);
		assertEquals(goodPositions[0][1], 0);
		assertEquals(goodPositions[1][0], 4);
		assertEquals(goodPositions[1][1], 3);

		int[][] byePositions = results.get("BYE");
		assertEquals(byePositions[0][0], 1);
		assertEquals(byePositions[0][1], 3);
		assertEquals(byePositions[1][0], 1);
		assertEquals(byePositions[1][1], 1);
	}

	@Test
	public void testRectangularInput() {
		GridSpecController gridSpecController = new GridSpecController(getInputFromFile("src", "test", "resources", "5x4_sample.txt"));
		Map<String, int[][]> results = gridSpecController.findWords();
		assertTrue(results.keySet().contains("HELO"));
		assertTrue(results.keySet().contains("GOOD"));
		assertTrue(results.keySet().contains("BYE"));
		assertEquals(results.keySet().size(), 3);

		int[][] heloPositions = results.get("HELO");
		assertEquals(heloPositions[0][0], 0);
		assertEquals(heloPositions[0][1], 0);
		assertEquals(heloPositions[1][0], 3);
		assertEquals(heloPositions[1][1], 3);

		int[][] goodPositions = results.get("GOOD");
		assertEquals(goodPositions[0][0], 4);
		assertEquals(goodPositions[0][1], 0);
		assertEquals(goodPositions[1][0], 4);
		assertEquals(goodPositions[1][1], 3);

		int[][] byePositions = results.get("BYE");
		assertEquals(byePositions[0][0], 1);
		assertEquals(byePositions[0][1], 3);
		assertEquals(byePositions[1][0], 1);
		assertEquals(byePositions[1][1], 1);
	}

	@Test
	public void testCornerCases() {
		GridSpecController gridSpecController = new GridSpecController(getInputFromFile("src", "test", "resources", "corner_cases.txt"));
		Map<String, int[][]> results = gridSpecController.findWords();

		assertTrue(results.keySet().contains("HELLO"));
		assertTrue(results.keySet().contains("HELLI"));
		assertTrue(results.keySet().contains("OHAIT"));
		assertTrue(results.keySet().contains("INUIT"));
		assertTrue(results.keySet().contains("OLIZI"));
		assertTrue(results.keySet().contains("HAIT"));
		assertTrue(results.keySet().contains("AIT"));
		assertTrue(results.keySet().contains("IT"));
		assertTrue(results.keySet().contains("T"));
		assertTrue(results.keySet().contains("ELLO"));
		assertTrue(results.keySet().contains("LLO"));
		assertTrue(results.keySet().contains("LO"));
		assertTrue(results.keySet().contains("O"));
		assertEquals(results.keySet().size(), 13);

		int[][] currPositions = results.get("HELLO");
		assertEquals(currPositions[0][0], 0);
		assertEquals(currPositions[0][1], 0);
		assertEquals(currPositions[1][0], 0);
		assertEquals(currPositions[1][1], 4);

		currPositions = results.get("HELLI");
		assertEquals(currPositions[0][0], 0);
		assertEquals(currPositions[0][1], 0);
		assertEquals(currPositions[1][0], 4);
		assertEquals(currPositions[1][1], 0);

		currPositions = results.get("OHAIT");
		assertEquals(currPositions[0][0], 0);
		assertEquals(currPositions[0][1], 4);
		assertEquals(currPositions[1][0], 4);
		assertEquals(currPositions[1][1], 4);

		currPositions = results.get("INUIT");
		assertEquals(currPositions[0][0], 4);
		assertEquals(currPositions[0][1], 0);
		assertEquals(currPositions[1][0], 4);
		assertEquals(currPositions[1][1], 4);

		currPositions = results.get("OLIZI");
		assertEquals(currPositions[0][0], 0);
		assertEquals(currPositions[0][1], 4);
		assertEquals(currPositions[1][0], 4);
		assertEquals(currPositions[1][1], 0);

		currPositions = results.get("HAIT");
		assertEquals(currPositions[0][0], 1);
		assertEquals(currPositions[0][1], 4);
		assertEquals(currPositions[1][0], 4);
		assertEquals(currPositions[1][1], 4);

		currPositions = results.get("AIT");
		assertEquals(currPositions[0][0], 2);
		assertEquals(currPositions[0][1], 4);
		assertEquals(currPositions[1][0], 4);
		assertEquals(currPositions[1][1], 4);

		currPositions = results.get("IT");
		assertEquals(currPositions[0][0], 3);
		assertEquals(currPositions[0][1], 4);
		assertEquals(currPositions[1][0], 4);
		assertEquals(currPositions[1][1], 4);

		currPositions = results.get("T");
		assertEquals(currPositions[0][0], 4);
		assertEquals(currPositions[0][1], 4);
		assertEquals(currPositions[1][0], 4);
		assertEquals(currPositions[1][1], 4);

		currPositions = results.get("ELLO");
		assertEquals(currPositions[0][0], 0);
		assertEquals(currPositions[0][1], 1);
		assertEquals(currPositions[1][0], 0);
		assertEquals(currPositions[1][1], 4);

		currPositions = results.get("LLO");
		assertEquals(currPositions[0][0], 0);
		assertEquals(currPositions[0][1], 2);
		assertEquals(currPositions[1][0], 0);
		assertEquals(currPositions[1][1], 4);

		currPositions = results.get("LO");
		assertEquals(currPositions[0][0], 0);
		assertEquals(currPositions[0][1], 3);
		assertEquals(currPositions[1][0], 0);
		assertEquals(currPositions[1][1], 4);

		currPositions = results.get("O");
		assertEquals(currPositions[0][0], 0);
		assertEquals(currPositions[0][1], 4);
		assertEquals(currPositions[1][0], 0);
		assertEquals(currPositions[1][1], 4);
	}

	@Test
	public void testSearch() {
		GridSpecController gridSpecController = new GridSpecController(getSampleInput());
		GridSpec currGridSpec = gridSpecController.getGridSpec();
		Map<Character, int[][]> charMap = currGridSpec.getCharacterMap();

		// DOWN
		int[][] result = gridSpecController.search(currGridSpec, charMap, "HGJCG");
		assertEquals(result[0][0], 0);
		assertEquals(result[0][1], 0);
		assertEquals(result[1][0], 4);
		assertEquals(result[1][1], 0);

		// DIAG_DL
		result = gridSpecController.search(currGridSpec, charMap, "FBLVG");
		assertEquals(result[0][0], 0);
		assertEquals(result[0][1], 4);
		assertEquals(result[1][0], 4);
		assertEquals(result[1][1], 0);

		// DIAG_UR
		result = gridSpecController.search(currGridSpec, charMap, "GVLBF");
		assertEquals(result[0][0], 4);
		assertEquals(result[0][1], 0);
		assertEquals(result[1][0], 0);
		assertEquals(result[1][1], 4);

		// DOWN
		result = gridSpecController.search(currGridSpec, charMap, "AEKVO");
		assertEquals(result[0][0], 0);
		assertEquals(result[0][1], 1);
		assertEquals(result[1][0], 4);
		assertEquals(result[1][1], 1);

		// LEFT
		result = gridSpecController.search(currGridSpec, charMap, "NLBVC");
		assertEquals(result[0][0], 3);
		assertEquals(result[0][1], 4);
		assertEquals(result[1][0], 3);
		assertEquals(result[1][1], 0);

		// UP
		result = gridSpecController.search(currGridSpec, charMap, "DLZBD");
		assertEquals(result[0][0], 4);
		assertEquals(result[0][1], 3);
		assertEquals(result[1][0], 0);
		assertEquals(result[1][1], 3);

		// DIAG_UL
		result = gridSpecController.search(currGridSpec, charMap, "OLLEH");
		assertEquals(result[0][0], 4);
		assertEquals(result[0][1], 4);
		assertEquals(result[1][0], 0);
		assertEquals(result[1][1], 0);

		// DIAG_RD
		result = gridSpecController.search(currGridSpec, charMap, "HELLO");
		assertEquals(result[0][0], 0);
		assertEquals(result[0][1], 0);
		assertEquals(result[1][0], 4);
		assertEquals(result[1][1], 4);

		// RIGHT
		result = gridSpecController.search(currGridSpec, charMap, "GEYBH");
		assertEquals(result[0][0], 1);
		assertEquals(result[0][1], 0);
		assertEquals(result[1][0], 1);
		assertEquals(result[1][1], 4);
	}

	@Test
	public void testSpaceInWord() {
		GridSpecController gridSpecController = new GridSpecController(getInputFromFile("src", "test", "resources", "9x1_space_test.txt"));
		Map<String, int[][]> results = gridSpecController.findWords();

		assertTrue(results.keySet().contains("TEST SPACE"));
		assertEquals(results.keySet().size(), 1);

		int[][] positions = results.get("TEST SPACE");
		assertEquals(positions[0][0], 0);
		assertEquals(positions[0][1], 0);
		assertEquals(positions[1][0], 8);
		assertEquals(positions[1][1], 0);
	}

	@Test
	public void testTabInWord() {
		GridSpecController gridSpecController = new GridSpecController(getInputFromFile("src", "test", "resources", "9x1_tab_test.txt"));
		Map<String, int[][]> results = gridSpecController.findWords();

		assertTrue(results.keySet().contains("TEST\tTAB"));
		assertEquals(results.keySet().size(), 1);

		int[][] positions = results.get("TEST\tTAB");
		assertEquals(positions[0][0], 0);
		assertEquals(positions[0][1], 0);
		assertEquals(positions[1][0], 6);
		assertEquals(positions[1][1], 0);
	}
}