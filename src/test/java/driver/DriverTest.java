package driver;

import base.TestBase;

import java.nio.file.Paths;

import org.junit.Test;
import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import controller.GridSpecController;

import java.util.List;

public class DriverTest extends TestBase {
	String[] args = {Paths.get("src", "test", "resources", "sample.txt").toString()};

	@Test
	public void testNoArgs() {
		Driver driver = new Driver();
		try {
			driver.main(new String[] {});
		} catch(RuntimeException rte){
			assertEquals(rte.getMessage(), "Number of arguments must be equal to one");
			return;
		}
		fail();
	}

	@Test
	public void testMoreThanOneArg() {
		Driver driver = new Driver();
		try {
			driver.main(new String[] {"asd", "def"});
		} catch(RuntimeException rte){
			assertEquals(rte.getMessage(), "Number of arguments must be equal to one");
			return;
		}
		fail();
	}

	@Test
	public void testFileDoesNotExist() {
		Driver driver = new Driver();
		try {
			driver.main(new String[] {"file/does/not/exist"});
		} catch(RuntimeException rte) {
			assertEquals(rte.getMessage(), "File does not exist at location file/does/not/exist");
			return;
		}
		fail();
	}

	@Test
	public void testGridSpecController(){
		List<String> sampleInput = getSampleInput();
		GridSpecController mockGSC = mock(GridSpecController.class);
		when(mockGSC.getGridSpec()).thenReturn(getSampleGridSpec());
		Driver driver = new Driver();
		driver.setGridSpecController(mockGSC);
		driver.main(args);
		verify(mockGSC, times(1)).findWords();
	}
}